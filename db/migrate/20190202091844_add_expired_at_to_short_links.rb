class AddExpiredAtToShortLinks < ActiveRecord::Migration[5.2]
  def change
    add_column :short_links, :expired_at, :datetime
  end
end

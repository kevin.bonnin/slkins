class CreateClickLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :click_logs do |t|
      t.integer :short_link_id

      t.timestamps
    end

    add_index :click_logs, :short_link_id
  end
end

class CreateShortLinks < ActiveRecord::Migration[5.2]
  def change
    create_table :short_links do |t|
      t.string :url
      t.string :slug
      t.integer :user_id
      t.inet :original_ip

      t.timestamps
    end

    add_index :short_links, :url
    add_index :short_links, :slug, unique: true
  end
end

import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueMq from 'vue-mq'
import Vuex from 'vuex'
import router from '@/router'
import SignIn from '@/views/SignIn'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueMq)

describe('SignIn', () => {
  beforeAll(() => {
    Object.defineProperty(window, "matchMedia", {
      value: jest.fn(() => {
        return {
          matches: true,
          addListener: jest.fn(),
          removeListener: jest.fn()
        }
      })
    });
  });

  it('redirects to root when user is already signed in', () => {
    let getters = {
      signedIn: (() => true),
      activePage: (() => '')
    }

    let actions = {
      updateActivePage: jest.fn()
    }

    let store = new Vuex.Store({ getters, actions })

    const wrapper = shallowMount(SignIn, { store, router, localVue })
    expect(window.location.pathname).toBe("/")
  })
})

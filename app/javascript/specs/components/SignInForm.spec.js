import { shallowMount } from '@vue/test-utils'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import SignInForm from '@/components/SignInForm'

describe('SignInForm', () => {
  it('displays an error alert if login is unsuccessful', () => {
    var mock = new MockAdapter(axios)
    const data = { error: 'Invalid Credentials' }

    mock.onPost().reply(401, data)

    const wrapper = shallowMount(SignInForm)
    const submit = wrapper.find('button[type="submit"]')
    submit.trigger('click')

    expect(wrapper.contains('.alert-error')).toBeTruthy
  })

  it('redirects to home page if login is successful', () => {
    var mock = new MockAdapter(axios)
    const data = { token: 'token' }

    mock.onPost().reply(201, data)

    const wrapper = shallowMount(SignInForm)
    const submit = wrapper.find('button[type="submit"]')
    submit.trigger('click')

    expect(window.location.pathname).toBe('/')
  })
})

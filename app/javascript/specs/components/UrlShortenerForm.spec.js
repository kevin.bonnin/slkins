import { shallowMount } from '@vue/test-utils'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import UrlShortenerForm from '@/components/UrlShortenerForm'

describe('UrlShortenerForm', () => {
  it('displays a success alert with the right content when request is successful', () => {
    var mock   = new MockAdapter(axios)
    const data = { short_url: 'short_url' }

    mock.onPost().reply(201, data)

    const wrapper = shallowMount(UrlShortenerForm)
    const submit = wrapper.find('button[type="submit"]')
    submit.trigger('click')

    expect(wrapper.contains('.alert-success')).toBeTruthy
  })

  it('displays an error alert with the right content when request is successful', () => {
    var mock   = new MockAdapter(axios)
    const data = { error: 'short_url' }

    mock.onPost().reply(422, data)

    const wrapper = shallowMount(UrlShortenerForm)
    const submit = wrapper.find('button[type="submit"]')
    submit.trigger('click')

    expect(wrapper.contains('.alert-error')).toBeTruthy
  })
})

import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueMq from 'vue-mq'
import Vuex from 'vuex'
import router from '@/router'
import Navbar from '@/components/Navbar'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueMq)

describe('Navbar', () => {
  beforeAll(() => {
    Object.defineProperty(window, "matchMedia", {
      value: jest.fn(() => {
        return {
          matches: true,
          addListener: jest.fn(),
          removeListener: jest.fn()
        }
      })
    });
  });

  it('displays the login button when the user is not signed in', () => {
    let getters = {
      signedIn: (() => false),
      activePage: (() => 'root')
    }

    let store = new Vuex.Store({ getters })

    const wrapper = shallowMount(Navbar, { store, router, localVue })
    expect(wrapper.contains(".logout-btn")).toBeFalsy()
    expect(wrapper.contains(".login-btn")).toBeTruthy
  })

  it('displays the logout button when the user is signed in', () => {
    let getters = {
      signedIn: (() => true),
      activePage: (() => 'root')
    }

    let store = new Vuex.Store({ getters })

    const wrapper = shallowMount(Navbar, { store, router, localVue })
    expect(wrapper.contains(".logout-btn")).toBeTruthy()
    expect(wrapper.contains(".login-btn")).toBeFalsy()
  })
})

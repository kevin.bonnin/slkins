import User from '@/models/User'

describe("User", () => {
  it('creates a new user from a Jwt', () => {
    const jwt = 'eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6ImFkbWluIiwiZXhwIjoxNTQ5OTE4NTQyfQ.HC14ZZoZa3azJfnm9oZ0TqblX0Xu7R8emB7Jw4IRT-k'
    const user = User.from(jwt)

    expect(user.username).toBe('admin')
  })
})

import * as Mutations from '@/mutationTypes/navigation'
import storeConfig from '@/modules/navigation'

describe('navigation mutation', () => {
  it('sets the active page to the passed parameter', () => {
    let state = { activePage: 'root' }

    storeConfig.mutations[Mutations.UPDATE_ACTIVE_PAGE](state, 'page')

    expect(state.activePage).toBe('page')
  })
})

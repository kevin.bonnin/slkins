import MockDate from 'mockdate'
import * as Mutations from '@/mutationTypes/auth'
import storeConfig from '@/modules/auth'
import localStorage from '../localStorage'
import axios from '@/axios'

window.localStorage = localStorage

describe('login mutations', () => {
  it('sets the right user from payload', () => {
    let state = { user: null }
    const jwt = 'eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6ImFkbWluIiwiZXhwIjoxNTQ5OTE4NTQyfQ.HC14ZZoZa3azJfnm9oZ0TqblX0Xu7R8emB7Jw4IRT-k'
    let payload = { token: jwt }

    storeConfig.mutations[Mutations.LOGIN](state, payload)

    expect(state.user.username).toBe('admin')
    expect(window.localStorage.token).toBe(jwt)
  })
  it('clears localStorage and state on logout', () => {
    let state = { user: { username: 'admin' } }
    window.localStorage.token = 'token'
    storeConfig.mutations[Mutations.LOGOUT](state)

    expect(window.localStorage.token).toBe(undefined)
    expect(state.user).toBe(null)
  })
  it('does not refresh token unless it expires in less than 24h', () => {
    let state = {
      user: { username: 'admin', authExpiry: new Date('2019-02-05 17:00:00') }
    }
    axios.patch = jest.fn(() => {
      return new Promise((resolve, reject) => {
        resolve({ data: { token: 'new_token' } })
      })
    })

    MockDate.set('2019-02-03 17:00:00')
    storeConfig.mutations[Mutations.REFRESH_JWT](state)
    expect(axios.patch).not.toBeCalled()

    MockDate.set('2019-02-04 17:00:00')
    storeConfig.mutations[Mutations.REFRESH_JWT](state)
    expect(axios.patch).toBeCalledWith("/sessions")
  })
})

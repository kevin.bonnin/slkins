import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/views/Home.vue'
import SignIn from '@/views/SignIn.vue'
import NotFound from '@/views/NotFound.vue'

import short_links from '@/routes/short_links'

Vue.use(Router)

const baseRoutes = [
  {
    path: '/',
    name: 'root',
    component: Home
  },
  {
    path: '/sign-in',
    name: 'signIn',
    component: SignIn
  },
  {
    path: "/404",
    component: NotFound
  },
  { path: '*', redirect: '/404' }
]

export default new Router({
  mode: 'history',
  routes: baseRoutes
    .concat(short_links)
})

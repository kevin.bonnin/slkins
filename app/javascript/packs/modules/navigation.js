import * as Mutations from '@/mutationTypes/navigation'

const state = {
  activePage: 'root'
}

const getters = {
  activePage (state) {
    return state.activePage
  }
}

const mutations = {
  [Mutations.UPDATE_ACTIVE_PAGE] (state, page) {
    state.activePage = page
  }
}

const actions = {
  updateActivePage ({ commit }, page) {
    commit(Mutations.UPDATE_ACTIVE_PAGE, page)
  }
}

export default {
  state,
  mutations,
  getters,
  actions
}

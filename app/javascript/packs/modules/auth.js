import User from "@/models/User"
import * as Mutations from '@/mutationTypes/auth'
import axios from "@/axios"

const state = {
  user: User.from(localStorage.token)
}

const getters = {
  currentUser (state) {
    return state.user
  },
  signedIn (state) {
    return !!state.user
  }
}

const mutations = {
  [Mutations.LOGIN] (state, payload) {
    localStorage.token = payload.token
    state.user = User.from(payload.token)
  },
  [Mutations.LOGOUT] (state) {
    delete localStorage.token
    state.user = null
  },
  [Mutations.REFRESH_JWT] (state) {
    if (state.user && (Math.abs((new Date) - state.user.authExpiry) / 36e5) <= 24) {
      axios.patch("/sessions")
      .then( response => {
        localStorage.token = response.data.token
        state.user = User.from(response.data.token)
      })
      .catch( () => {
        delete localStorage.token
        state.user = null
      })
    }
  }
}

const actions = {
  login ({ commit }, payload) {
    commit(Mutations.LOGIN, payload)
  },
  logout ({ commit }) {
    commit(Mutations.LOGOUT)
  },
  refreshJwt ({ commit }) {
    commit(Mutations.REFRESH_JWT)
  }
}

export default {
  state,
  mutations,
  getters,
  actions
}

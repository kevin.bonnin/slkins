import axios from 'axios'

const token = document.querySelector('[name="csrf-token"]') || { content: 'no-csrf-token' }

let customAxios = axios.create({
  headers: { common: { 'X-CSRF-Token': token.content } }
})

customAxios.interceptors.request.use(
  (config) => {
    let jwt = localStorage.token

    if (!!jwt) {
      config.headers['Authorization'] = `Bearer ${localStorage.token}`
    }

    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

export default customAxios

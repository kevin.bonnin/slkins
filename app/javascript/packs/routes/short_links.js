import Index from '@/views/short_links/Index.vue'
import Show from '@/views/short_links/Show.vue'

export default [
  {
    path: "/my_slinks",
    component: Index,
    name: "short_links-index",
    children: [
      {
        path: ":id",
        component: Show,
        name: "short_links-show"
      }
    ]
  }
]

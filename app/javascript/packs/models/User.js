import JwtDecode from 'jwt-decode'

export default class User {
  static from (token) {
    try {
      let obj = JwtDecode(token)
      return new User(obj)
    } catch (_) {
      return null
    }
  }

  constructor ({ user_id, username, exp }) {
    this.id = user_id
    this.username = username
    this.authExpiry = new Date(exp * 1000)
  }
}

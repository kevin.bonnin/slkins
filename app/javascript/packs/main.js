import Vue from 'vue'
import VueMq from 'vue-mq'
import VueAxios from 'vue-axios'
import VueClipboard from 'vue-clipboard2'
import VueMoment from 'vue-moment'
import axios from '@/axios'
import App from '@/App.vue'
import router from '@/router'
import store from '@/store'

Vue.config.productionTip = false

Vue.use(VueMq, {
  breakpoints: {
    xs: 450,
    sm: 768,
    md: 990,
    lg: Infinity
  }
})

Vue.use(VueAxios, axios)
Vue.use(VueClipboard)
Vue.use(VueMoment)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')

module Authentication
  class AuthenticateUserFromAuthorizationHeader
    include Interactor::Organizer

    organize ExtractJwtFromAuthorizationHeader,
             DecodeJwt,
             AuthenticateUserFromJwt
  end
end

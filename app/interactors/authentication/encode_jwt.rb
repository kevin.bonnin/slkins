module Authentication
  class EncodeJwt
    include Interactor

    def call
      context.jwt = JWT.encode(
        context.payload,
        Rails.application.credentials.secret_key_base,
        'HS256'
      )
    end
  end
end

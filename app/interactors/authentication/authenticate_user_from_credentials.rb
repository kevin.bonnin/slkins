module Authentication
  class AuthenticateUserFromCredentials
    include Interactor

    def call
      context.user = User.find_by(username: context.username)

      return if context.user&.authenticate(context.password)

      context.fail!(error: 'Identifiants Invalides')
    end
  end
end

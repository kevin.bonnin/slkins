module Authentication
  class DecodeJwt
    include Interactor

    def call
      body = JWT.decode(
        context.jwt,
        Rails.application.credentials.secret_key_base,
        true,
        algorithm: 'HS256'
      )
      context.decoded_jwt = Hashie::Mash.new(body&.first)
    rescue JWT::ExpiredSignature
      context.fail!(error: "Token d'authentification expiré")
    rescue JWT::DecodeError => e
      context.fail!(error: e.message)
    end
  end
end

module Authentication
  class CreateUserAccountPayload
    include Interactor

    def call
      context.payload = {
        user_id: context.user.id,
        username: context.user.username,
        exp: 1.week.from_now.to_i
      }
    end
  end
end

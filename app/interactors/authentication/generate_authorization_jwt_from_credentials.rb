module Authentication
  class GenerateAuthorizationJwtFromCredentials
    include Interactor::Organizer

    organize AuthenticateUserFromCredentials,
             CreateUserAccountPayload,
             EncodeJwt
  end
end

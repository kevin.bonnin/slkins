module Authentication
  class ExtractJwtFromAuthorizationHeader
    include Interactor

    def call
      context.jwt = context.authorization_header.split(' ').last

      return if context.jwt.present?

      context.fail!(error: "Token d'authentification absent")
    end
  end
end

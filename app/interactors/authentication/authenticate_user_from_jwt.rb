module Authentication
  class AuthenticateUserFromJwt
    include Interactor

    def call
      context.user = User.find_by(id: context.decoded_jwt&.user_id)

      return if context.user

      context.fail!(error: 'JWT invalide')
    end
  end
end

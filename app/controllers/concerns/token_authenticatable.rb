class NotAuthorizedException < StandardError; end

module TokenAuthenticatable
  extend ActiveSupport::Concern

  included do
    attr_reader :current_user

    before_action :authenticate_user

    rescue_from NotAuthorizedException, with: -> {
      render json: { error: 'Not Authorized' }, status: :unauthorized
    }
  end

  def current_user
    @current_user
  end

  def user_signed_in?
    @current_user.present?
  end

  private

    def authenticate_user
      return if request.headers['Authorization'].blank?

      authentication = Authentication::AuthenticateUserFromAuthorizationHeader
      authentication = authentication.call(
        authorization_header: request.headers['Authorization']
      )

      @current_user = authentication.user if authentication.success?
    end

    def authenticate_user!
      authenticate_user
      raise NotAuthorizedException unless @current_user
    end
end


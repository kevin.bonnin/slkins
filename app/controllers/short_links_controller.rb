class ShortLinksController < ApplicationController
  include TokenAuthenticatable

  before_action :load_short_link, only: %i(update destroy)

  def index
    @short_links = if user_signed_in?
                     ShortLink.all.order(expired_at: :desc)
                   else
                     ShortLink.from_ip(request.remote_ip).order(expired_at: :desc)
                   end

    search_params = {
      url_cont: params[:search]
    }

    page = params[:page].presence || 1

    @short_links = @short_links.ransack(search_params).result.paginate(page: page)
    @short_links = @short_links.preload(:click_logs)
    render json: @short_links, meta: paginate(@short_links)
  end

  def show
    @short_link = ShortLink.active.find_by(slug: params[:id])
    return render inline: '', layout: 'application' unless @short_link

    @short_link.click_logs.create
    redirect_to @short_link.url
  end

  def create
    @short_link = ShortLink.create(short_link_params)

    if @short_link.persisted?
      render json: @short_link, status: :created
    else
      error = { errors: @short_link.errors.full_messages }
      render json: error, status: :unprocessable_entity
    end
  end

  def update
    if @short_link.update(short_link_params)
      render json: @short_link
    else
      error = { errors: @short_link.errors.full_messages }
      render json: error, status: :unprocessable_entity
    end
  end

  def destroy
    if @short_link.destroy
      head :ok
    else
      head :unprocessable_entity
    end
  end

  private

    def load_short_link
      @short_link = ShortLink.find(params[:id])
    end

    def short_link_params
      params.require(:short_link).permit(:url, :duration).merge(user_params)
    end

    def user_params
      return { user: current_user } if user_signed_in?
      { original_ip: request.remote_ip }
    end

    def paginate(resource)
      {
        pagination: {
          per_page: resource.per_page,
          total_pages: resource.total_pages,
          total_objects: resource.total_entries
        }
      }
    end
end

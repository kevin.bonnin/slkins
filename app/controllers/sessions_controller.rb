class SessionsController < ApplicationController
  include TokenAuthenticatable

  skip_before_action :authenticate_user, only: %i(create)

  def create
    result = Authentication::GenerateAuthorizationJwtFromCredentials.call(user_params)
    return render json: { token: result.jwt }, status: :created if result.success?

    render json: { error: result.error }, status: :unauthorized
  end

  def update
    return render json: { error: 'Accès refusé' }, status: :unauthorized unless user_signed_in?
    render json: { token: Authentication::GenerateAuthorizationJwt.call(user_account: current_user).jwt }, status: :ok
  end

  private

    def user_params
      params.require(:user).permit(:username, :password)
    end
end

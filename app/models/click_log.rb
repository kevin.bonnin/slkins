class ClickLog < ApplicationRecord
  belongs_to :short_link

  scope :for_period, -> (p_start, p_end) {
    where(created_at: p_start...p_end)
  }
end

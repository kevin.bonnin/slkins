class ShortLink < ApplicationRecord
  # NOTE : expressed in days but used as minutes for the purpose of this exercise
  DEFAULT_DURATION          = 3.freeze
  MAX_REGISTERED_DURATION   = 30.freeze
  MAX_UNREGISTERED_DURATION = 3.freeze

  belongs_to :user, optional: true
  has_many :click_logs, dependent: :destroy

  before_validation :generate_slug, on: :create
  before_validation :set_expiration

  validates :url, presence: true, url: { no_local: true }
  validates :original_ip, presence: true, if: -> { user_id.blank? }
  validates :slug, presence: true, uniqueness: true

  validate :allowed_duration?
  validate :duplicate?, on: :create

  attr_accessor :duration

  scope :from_ip, -> (ip) { where(original_ip: ip) }
  scope :active, -> { where('expired_at >= ?', Time.current) }

  private

    def allowed_duration?
      max = user_id ? MAX_REGISTERED_DURATION : MAX_UNREGISTERED_DURATION

      return if duration.to_i <= max
      errors.add(:duration, :not_allowed)
    end

    def set_expiration
      self.duration ||= DEFAULT_DURATION
      start = created_at || Time.current

      self.expired_at = if (start + duration.to_i.days < Time.current)
                          diff = (Time.current.to_i - start.to_i) / 1.day
                          start + diff.minutes
                        else
                          start + duration.to_i.minutes
                        end
    end

    def generate_slug
      return if slug.present?

      self.slug = loop do
        random_slug = [*('a'..'z'),*('0'..'9')].shuffle[0,8].join
        break random_slug unless ShortLink.exists?(slug: random_slug)
      end
    end

    def duplicate?
      existing_url = ShortLink.active.find_by(url: url, user_id: user_id)
      existing_url ||= ShortLink.active.find_by(url: url, original_ip: original_ip)

      return unless existing_url

      errors.add(
        :url,
        "is already shortened.<br>Please consider extending the validity of"\
        "<a href='/my_slinks?search=#{existing_url.url}' class='alert-link'>this link</a>"
      )
    end
end

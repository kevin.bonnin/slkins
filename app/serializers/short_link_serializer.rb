class ShortLinkSerializer < ActiveModel::Serializer
  attributes :id, :url, :short_url, :click_logs, :created_at, :expired_at, :validity

  def validity
    (object.expired_at.to_i - object.created_at.to_i).to_f / 1.minute
  end

  # NOTE we override expired_at to take into account that the test is working
  # in minutes instead of days
  def expired_at
    object.created_at + validity.days
  end

  def short_url
    "#{Rails.application.routes.url_helpers.root_url}#{object.slug}"
  end

  def click_logs
    exp = [object.expired_at, Time.current].min
    range = (object.created_at.to_i)..(exp.to_i)

    {}.tap do |hash|
      (range).step(1.minute).each_with_index do |time, i|
        date = (object.created_at + i.days).to_date
        period = [Time.at(time), Time.at(time) + 1.minute]
        hash[date] = object.click_logs.for_period(*period).count
      end
    end
  end
end

RSpec.describe ShortLink, type: :model do
  let(:short_link) { build(:short_link) }

  subject { short_link }

  it { is_expected.to validate_presence_of(:url) }
  it { is_expected.to validate_uniqueness_of(:slug) }

  describe "it validates that ip is present if user is unregistered" do
    context "when user is registered" do
      let(:user) { create(:user) }
      let(:short_link) { build(:short_link, user: user) }

      it { is_expected.not_to validate_presence_of(:original_ip) }
    end

    context "when user is not registered" do
      it { is_expected.to validate_presence_of(:original_ip) }
    end
  end

  it "automatically generates a slug" do
    expect(create(:short_link).slug).to be_present
  end

  describe "automatically sets the expiration time" do
    around do |example|
      Timecop.freeze(Time.current) do
        example.run
      end
    end

    context "when no duration is passed" do
      let!(:short_link) { create(:short_link) }

      it "sets the expiration time to default duration" do
        expired_at = Time.current + ShortLink::DEFAULT_DURATION.minutes
        expect(short_link.expired_at).to eq(expired_at)
      end
    end

    context "when a valid duration is passed for unregistered user" do
      let!(:short_link) { create(:short_link, duration: 2) }

      it "sets the expiration time to the duration"do
        expired_at = Time.current + 2.minutes
        expect(short_link.expired_at).to eq(expired_at)
      end
    end

    context "when a valid duration is passed for unregistered user" do
      let(:user) { create(:user) }
      let!(:short_link) { create(:short_link, user: user, duration: 20) }

      it "sets the expiration time to the duration"do
        expired_at = Time.current + 20.minutes

        expect(short_link).to be_valid
        expect(short_link.expired_at).to eq(expired_at)
      end
    end

    context "when an invalid duration is passed" do
      it { expect(build(:short_link, duration: 20)).not_to be_valid }
    end
  end
end

RSpec.describe User, type: :model do
  subject { build(:user) }

  it { is_expected.to validate_presence_of(:username) }
end

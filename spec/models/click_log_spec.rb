RSpec.describe ClickLog, type: :model do
  subject { build(:click_log) }

  it { is_expected.to belong_to(:short_link) }

  describe "#for_period" do
    let(:short_link) { create(:short_link) }

    before do
      Timecop.freeze(Time.current - 3.minutes) do
        create_list(:click_log, 3, short_link: short_link)
      end

      Timecop.freeze(Time.current) do
        create_list(:click_log, 5, short_link: short_link)
      end
    end

    around do |example|
      Timecop.freeze(Time.current) do
        example.run
      end
    end

    it "retrieves the right logs for each period" do
      t = Time.current
      expect(ClickLog.for_period(t - 3.minutes, t - 2.minutes).count).to eq(3)
      expect(ClickLog.for_period(t, t + 1.minute).count).to eq(5)
      expect(ClickLog.for_period(t - 1.minute, t).count).to eq(0)
    end
  end
end

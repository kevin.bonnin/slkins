FactoryBot.define do
  factory :short_link do
    url { Faker::Internet.url }
    original_ip { Faker::Internet.ip_v4_address }
  end
end

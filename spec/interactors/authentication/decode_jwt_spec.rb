RSpec.describe Authentication::EncodeJwt do
  let(:user) { create(:user) }
  let(:payload) { { id: user.id, username: user.username } }
  let(:jwt) { Authentication::EncodeJwt.call(payload: payload).jwt }

  subject(:context) { Authentication::DecodeJwt.call(jwt: jwt) }

  it { expect(context).to be_a_success }

  it { expect(context.decoded_jwt.to_hash.symbolize_keys).to eq(payload) }
end

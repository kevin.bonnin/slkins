RSpec.describe Authentication::ExtractJwtFromAuthorizationHeader do
  subject(:context) {
    Authentication::ExtractJwtFromAuthorizationHeader.call(
      authorization_header: header
    )
  }

  context 'with a valid auth header' do
    let(:header) { 'Bearer auth_token' }

    it { expect(context).to be_a_success }
    it { expect(context.jwt).to eq('auth_token') }
  end

  context 'with an invalid auth header' do
    let(:header) { '' }

    it { expect(context).to be_a_failure }
    it { expect(context.error).to eq("Token d'authentification absent") }
    it { expect(context.jwt).to be_blank }
  end
end

RSpec.describe Authentication::AuthenticateUserFromJwt do
  let(:user) { create(:user, password: 'password') }

  let(:jwt) {
    Authentication::GenerateAuthorizationJwtFromCredentials.call(
      username: user.username,
      password: 'password'
    ).jwt
  }

  subject(:context) {
    Authentication::AuthenticateUserFromJwt.call(decoded_jwt: decoded_jwt)
  }

  context 'with valid jwt' do
    let(:decoded_jwt) { Authentication::DecodeJwt.call(jwt: jwt).decoded_jwt }

    it { expect(context).to be_a_success }
    it { expect(context.user).to eq(user) }
  end

  context 'with wrong jwt' do
    let(:decoded_jwt) { nil }

    it { expect(context).to be_a_failure }
    it { expect(context.user).to be_blank }
    it { expect(context.error).to eq('JWT invalide') }
  end
end

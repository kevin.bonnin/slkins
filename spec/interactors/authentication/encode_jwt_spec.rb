RSpec.describe Authentication::EncodeJwt do
  let(:user) { create(:user) }
  let(:payload) { { id: user.id, username: user.username } }

  subject(:context) { Authentication::EncodeJwt.call(payload: payload) }

  it { expect(context).to be_a_success }

  it 'can be decoded using the DecodeJwt Interactor' do
    decoded = Authentication::DecodeJwt.call(jwt: context.jwt)
    expect(decoded.decoded_jwt.to_hash.symbolize_keys).to eq(payload)
  end
end

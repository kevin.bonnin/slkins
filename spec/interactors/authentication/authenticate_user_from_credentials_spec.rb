RSpec.describe Authentication::AuthenticateUserFromCredentials do
  let(:user) { create(:user, password: 'password') }

  subject(:context) {
    Authentication::AuthenticateUserFromCredentials.call(
      username: user.username,
      password: password
    )
  }

  context 'with valid password' do
    let(:password) { 'password' }

    it { expect(context).to be_a_success }
    it { expect(context.user).to eq(user) }
  end

  context 'with wrong password' do
    let(:password) { 'drowssap' }

    it { expect(context).to be_a_failure }
    it { expect(context.error).to eq('Identifiants Invalides') }
  end
end

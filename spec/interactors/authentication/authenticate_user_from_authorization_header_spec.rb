RSpec.describe Authentication::AuthenticateUserFromAuthorizationHeader do
  let(:user) { create(:user, password: 'password') }
  let(:jwt) {
    Authentication::GenerateAuthorizationJwtFromCredentials.call(
      username: user.username,
      password: 'password'
    ).jwt
  }

  subject(:context) {
    Authentication::AuthenticateUserFromAuthorizationHeader.call(
      authorization_header: header
    )
  }

  context 'with valid auth header' do
    let(:header) { "Bearer #{jwt}" }

    it { expect(context).to be_a_success }
    it { expect(context.user).to eq(user) }
  end

  context 'with wrong auth header' do
    let(:header) { 'Bearer' }
    it { expect(context).to be_a_failure }
  end
end

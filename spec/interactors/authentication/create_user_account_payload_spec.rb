RSpec.describe Authentication::CreateUserAccountPayload do
  let(:user) { create(:user) }
  subject(:context) {
    Authentication::CreateUserAccountPayload.call(user: user)
  }

  it { expect(context).to be_a_success }

  it 'creates the correct payload' do
    expect(context.payload).to eq(
      user_id: user.id,
      username: user.username,
      exp: 1.week.from_now.to_i
    )
  end
end

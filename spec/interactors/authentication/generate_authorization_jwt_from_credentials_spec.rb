RSpec.describe Authentication::GenerateAuthorizationJwtFromCredentials do
  let(:user) { create(:user, password: 'password') }

  subject(:context) {
    Authentication::GenerateAuthorizationJwtFromCredentials.call(
      username: user.username,
      password: password
    )
  }

  context 'with valid password' do
    let(:password) { 'password' }

    it { expect(context).to be_a_success }
  end

  context 'with wrong password' do
    let(:password) { 'drowssap' }

    it { expect(context).to be_a_failure }
  end
end

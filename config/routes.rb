Rails.application.routes.draw do
  root to: 'application#index'

  resources :short_links, except: %i(show new edit)
  resources :sessions, only: %i(create update)

  get '/:id', to: 'short_links#show'
end

const { environment } = require('@rails/webpacker')
const vue =  require('./loaders/vue')
const customAliases = require("./customAliases")

environment.loaders.append('vue', vue)
environment.config.merge(customAliases)
module.exports = environment
